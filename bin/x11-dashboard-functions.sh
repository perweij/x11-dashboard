
function get_all_client_ids() {
    local DISP="$1"

    DISPLAY="$DISP" wmctrl -l | cut -d' ' -f 1 | sort
}



## returns two values: pid of the command's process, and the X11 client id of the process
function run_cmd() {
    local DISP="$1" ; shift
    local SPEED="$1" ; shift

    local IDS_BEF="$(get_all_client_ids "$DISP")"

    echo "**** starting $@" >&2
    DISPLAY="$DISP" "$@" >&2 </dev/null &
    CMDPID=$!

    local IDS_AFT="$(get_all_client_ids "$DISP")"

    while diff <(echo "$IDS_BEF") <(echo "$IDS_AFT") >/dev/null; do
        sleep 1
        local IDS_AFT="$(get_all_client_ids "$DISP")"
    done

    local CLIENT_ID=$(diff <(echo "$IDS_BEF") <(echo "$IDS_AFT") | grep '^>' | sed 's|^> ||')

    echo "$CMDPID" "$CLIENT_ID"
}



function mv_to_pane() {
    local PANE="$1"
    local ID="$2"

    if [ -z $ID ]; then
        echo "**** give pane and id as arguments" >&1
        exit 1
    fi

    ## Give the x11 client a class name we can use as a selector with xdotool
    xprop -id "${ID}" -f WM_CLASS 8s -set WM_CLASS "SELECT$ID"

    ## search for the selector, then move the client to the specified pane
    xdotool search --classname "SELECT$ID" set_window --role "${PANE}" windowunmap windowmap
}
