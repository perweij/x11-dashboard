#!/bin/bash
set -e # exit if a command fails

xhost +
vagrant up
echo -e "\n\n[ Set the opened window to fullscreen, in case it is not already ]\n"
vagrant ssh -c x11-dashboard
vagrant destroy
xhost -
